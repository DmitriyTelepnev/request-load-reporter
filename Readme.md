До запуска необходимо установить

	1) sudo apt install python3-pip

	2) pip3 install psycopg2

Чтобы запустить скрипт - нужно

	1) выдать разрешение на выполнение файлу RequestLoadReporter

	2) Прописать параметры подключения к БД

	3) прописать путь до лог-файла

	4) запустить: ./RequestLoadReporter.py