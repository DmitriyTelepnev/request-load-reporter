#!/usr/bin/python3 

from engine.LogParser import LogParser
from engine.Statistic import Statistic
from engine.StatisticRenderer import StatisticRenderer
from engine.DbMailSender import DbMailSender

class RequestLoadReporter:
	@staticmethod
	def main():
		statistic = Statistic( LogParser().parse() )
		statisticRenderer = StatisticRenderer( statistic )
		mailSender = DbMailSender()
		mailSender.send( statisticRenderer.renderHtml() )

RequestLoadReporter.main()