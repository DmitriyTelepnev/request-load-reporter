import json

class Configurator:

	_cfg = {}

	@staticmethod
	def getConfig(key):
		if key in Configurator._cfg.keys():
			return Configurator._cfg[key]
		else:
			print ("Нет такого ключа конфигурации")
			raise

with open("engine/config/main.json", 'r') as jsonFile:
	Configurator._cfg = json.load(jsonFile)