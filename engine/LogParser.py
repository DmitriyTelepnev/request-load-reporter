import re
import datetime

now = datetime.datetime.now()

from .Configurator import Configurator
from .entities.Request import Request

class LogParser:
	"""Класс-парсер запросов из логов"""

	REQUEST_REGEXP = r"(GET|POST|PUT|DELETE) \/[A-Za-z0-9_\-=+\/\.]*[\?\/][A-Za-z0-9\S]*" # Регулярка для запроса
	REQUEST_DURATION_REGEXP = r"[0-9]{1,}\.[0-9]{0,3}$" # регулярка для времени выполнения запроса
	REQUEST_TRAFFIC_REGEXP = r"HTTP\/1\.1\" [0-9]{3} [0-9]{1,}" # регулярка для получения траффика
	REQUEST_TRAFFIC_DELETE_TRASH_REGEXP = r"^HTTP\/1\.1\" [0-9]{3} " # регулярка для удаления из трафика лишнего мусора
	DELETED_REQUEST_METHOD_REGEXP = r"(GET|POST|PUT|DELETE) " # регулярка для удаления метода запроса
	DELETED_STATIC_SCRIPTS_REGEXP = r"\.(js[x]{0,1}|[s]{0,1}css|png|jp[e]{0,1}g|gif)" # регулярка для фильтрации статических скриптов
	EXCLUDED_REQUEST_PATHS = r" \/(files|reports|images|otForm|frontend|nginx_status|ws)\/" # регулярка для исключения некоторых запросов

	# публичный метод парсинга лога
	def parse(self):
		currentDateLog = self._parseCurrentDateLog()
		return self._parseRequests(currentDateLog)

	# Парсинг логов за сегодняшний день
	def _parseCurrentDateLog(self):
		accessLogFile = open( Configurator.getConfig("main")["accessLogPath"], 'r' )
		result = [];

		for accessLogLine in accessLogFile:
			filteredRequests = re.findall(self.EXCLUDED_REQUEST_PATHS, accessLogLine)
			if not filteredRequests and self._isCurrentDateDynamicRequest(accessLogLine):
				result.append(accessLogLine)

		accessLogFile.close()
		return result

	# фильтрация динамических запросов за сегодняшний день
	def _isCurrentDateDynamicRequest(self, accessLogLine):
		CURRENT_DATE_REGEXP = r"%s" % now.strftime("%d\/%b")
		currentDateLogLine = re.findall(CURRENT_DATE_REGEXP, accessLogLine)
		staticScriptsLine = re.findall(self.DELETED_STATIC_SCRIPTS_REGEXP, accessLogLine)

		return currentDateLogLine and not staticScriptsLine
		
	# парсинг уже отфильтрованных логов
	def _parseRequests(self, currentDateLog):
		requests = [];
		for accessLogLine in currentDateLog:
			resultLine = re.search(self.REQUEST_REGEXP, accessLogLine)
			if resultLine:
				request = self._createRequestObject(resultLine, accessLogLine)
				requests.append(request);
		
		return requests

	# метод-конструктор объекта Request
	def _createRequestObject(self, resultLine, accessLogLine):
		duration = self._getRequestDuration(accessLogLine)
		requestUrl = re.sub(self.DELETED_REQUEST_METHOD_REGEXP, "", resultLine.group(0))
		traffic = self._getRequestTraffic(accessLogLine)
		return Request(requestUrl, duration, traffic)

	# пполучение времени выполнения запроса из строки лога
	def _getRequestDuration(self, accessLogLine):
		durationResult = re.search(self.REQUEST_DURATION_REGEXP, accessLogLine)
		return durationResult and durationResult.group(0) or 0

	# пполучение траффика запроса из строки лога
	def _getRequestTraffic(self, accessLogLine):
		trafficResult = re.search(self.REQUEST_TRAFFIC_REGEXP, accessLogLine)
		trafficResult = trafficResult and re.sub(self.REQUEST_TRAFFIC_DELETE_TRASH_REGEXP, "", trafficResult.group(0))
		return trafficResult and int(trafficResult) or 0