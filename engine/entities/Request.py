class Request:
	"""Класс запроса с временем выполнения и переданным трафиком"""

	def __init__(self, request, duration, traffic):
		self._request = request
		self._duration = round(float(duration), 2)
		self._traffic = traffic

	def getRequest(self):
		return self._request

	def getDuration(self):
		return self._duration

	def getTraffic(self):
		return self._traffic

	def __str__(self):
		return "%s - %s - %s" % (self._request, self._traffic, self._duration)