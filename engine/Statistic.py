import re
from collections import OrderedDict

from .Configurator import Configurator

class Statistic:
	"""Подсчет статистики по логам"""

	DELETED_PARAMETER_VALUES_REGEXP = r"([0-9,]*|%[^\&]*\&) " # регулярка для удаления значений параметров из строки запроса
	OLD_URI_PARAMETER_KEYS_REGEXP = r"((f|p)=[A-Za-z]*)[\&]{0,1}" # регулярка для получения единственных важных параметров (f= и p=) из старой части АСУПа
	
	def __init__(self, parsedLog):
		self._parsedLog = parsedLog
		self._statistic = {}

		self.createPrettyStatistic()

	# сбор статистики
	def createPrettyStatistic(self):
		statistic = {};
		for request in self._parsedLog:
			cleanRequestUrl = self._getCleanedUrl(request.getRequest())
			if cleanRequestUrl in statistic.keys():
				existRequest = statistic[cleanRequestUrl];
				statistic[cleanRequestUrl] = {
					"time": existRequest['time'] + request.getDuration(),
					"quantity": existRequest["quantity"] + 1,
					"traffic": existRequest['traffic'] + request.getTraffic(),
				}
			else:
				statistic[cleanRequestUrl] = {
					"time": request.getDuration(),
					"quantity": 1,
					"traffic": request.getTraffic(),
				}

		self._statistic = statistic

	# очистка URL
	def _getCleanedUrl(self, url):
		cleanRequestUrl = re.sub(self.DELETED_PARAMETER_VALUES_REGEXP, "", url)

		clearedGetParameters = self._clearUriParameters(cleanRequestUrl)

		cleanRequestUrl = re.sub(r"\?\S+", "", cleanRequestUrl)
		cleanRequestUrl += ("?" + clearedGetParameters) if clearedGetParameters else ""
		return cleanRequestUrl

	def _clearUriParameters(self, cleanRequestUrl):
		CLEAR_URI_PARAMETERS_REGEXP = r"(?!(((f|p)=\S+)))"

		uriParameterSearch = re.search(self.OLD_URI_PARAMETER_KEYS_REGEXP, cleanRequestUrl)

		uriParameters = uriParameterSearch and uriParameterSearch.group(0) or ""
		
		return re.sub(CLEAR_URI_PARAMETERS_REGEXP, "", uriParameters)

	# сортирока статистики по параметру
	def _sortStatistic(self, statistic, sortByParam):
		sortedKeys = sorted(statistic, key=lambda x: statistic[x][sortByParam], reverse=True);
		sortedStatistic = OrderedDict()

		for key in sortedKeys:
			sortedStatistic[key] = statistic[key]

		return sortedStatistic

	# топ статистики по времени
	def getTopByTime(self):
		return self._getTop( self._sortStatistic( self._statistic, 'time' ) )

	# топ статистики по количеству запросов
	def getTopByQuantity(self):
		return self._getTop( self._sortStatistic( self._statistic, 'quantity' ) )

	# топ статистики по суммарному трафику
	def getTopByTraffic(self):
		return self._getTop( self._sortStatistic( self._statistic, 'traffic' ) )

	# сбор топ запросов
	def _getTop(self, statistic):
		top = OrderedDict()
		increment = 0
		for request in statistic:
			if increment >= Configurator.getConfig("main")["topLimit"]:
				break

			rq = self._statistic[request]
			increment += 1
			top[request] = {
				"time": rq["time"],
				"quantity": rq["quantity"],
				"traffic": rq['traffic']
			}

		return top