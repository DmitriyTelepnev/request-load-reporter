import datetime
from .Configurator import Configurator

now = datetime.datetime.now()

import psycopg2

class DbMailSender:
	"""Рассыльщик писем"""
	SUBJECT = now.strftime(Configurator.getConfig("mail")["subjectTemplate"])

	def __init__(self):
		try:
			dbConfig = Configurator.getConfig("db")
			self._conn = psycopg2.connect(
				"dbname='%s' user='%s' host='%s' password='%s' port='%s'"
				% (dbConfig["name"], dbConfig["user"], dbConfig["host"], dbConfig["password"], dbConfig["port"])
			)
		except:
			print ("Не могу подключиться к БД")
			raise

	def send(self, mail):
		mailConfig = Configurator.getConfig("mail")
		cur = self._conn.cursor()
		cur.execute(
			"SELECT mailsender.send_simple('%s', '%s', '%s', '%s', '%s')"
			% (mailConfig["sender"], mailConfig["source"], mailConfig["recipient"], self.SUBJECT, mail)
		)
		self._conn.commit()
		self._conn.close()