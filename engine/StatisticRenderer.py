from collections import OrderedDict

from .Configurator import Configurator

class StatisticRenderer:
	"""Отрисовщик красивых табличек со статистикой"""
	
	def __init__(self, statistic):
		self._statistic = statistic

	def renderHtml(self):
		tableStatisticByTime = self._renderStatisticTable( self._statistic.getTopByTime() );
		tableStatisticByRequestQuantity = self._renderStatisticTable( self._statistic.getTopByQuantity() )
		tableStatisticByRequestTraffic = self._renderStatisticTable( self._statistic.getTopByTraffic() )

		return """
				<style>
					table td, table th {
						vertical-align: top;
						border: 1px solid #888;
					}
				</style>
				<table>
					<tr>
						<th>По суммарному времени выполнения</th>
						<th>По количеству запросов</th>
						<th>По передаваемому траффику</th>
					</tr>
					<tr>
						<td>%s</td>
						<td>%s</td>
						<td>%s</td>
					</tr>
				</table>
			""" % (tableStatisticByTime, tableStatisticByRequestQuantity, tableStatisticByRequestTraffic)

	def _renderStatisticTable(self, statistic):
		mainConfig = Configurator.getConfig("main")

		tableRows = """
				<tr>
					<th>Запрос</th>
					<th>Суммарное время выполнения, мин</th>
					<th>Количество, шт</th>
					<th>Передаваемый траффик, Мб</th>
				</tr>
			"""
		for request in statistic.keys():
			requestStatistic = statistic[request]
			tableRows += ("""
					<tr>
						<td>%s</td>
						<td>%0.2f</td>
						<td>%s</td>
						<td>%0.2f</td>
					</tr>
				""" % (request, requestStatistic["time"] / mainConfig["timeMeasureDelimeter"], requestStatistic["quantity"], requestStatistic["traffic"] / mainConfig["trafficMeasureDelimeter"]))

		return "<table>%s</table>" % tableRows